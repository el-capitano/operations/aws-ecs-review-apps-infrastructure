locals {
  has_role = var.role_name != null
}

provider "aws" {
  region = "eu-west-1"

  dynamic "assume_role" {
    for_each = local.has_role ? toset([var.role_name]) : []
    content {
      role_arn     = "arn:aws:iam::${var.account_id}:role/${var.role_name}"
      session_name = var.session_name
    }
  }
}

provider "gitlab" {
  token = var.gitlab_token
}
