resource "gitlab_deploy_token" "ecs_pull_images" {
  group    = data.gitlab_group.el_capitano.full_path
  name     = "Used by ECS to retrieve images from Gitlab Container Registry"
  username = "gitlab_ecs_deploy_token"

  scopes = ["read_registry"]
}

resource "aws_secretsmanager_secret" "glcr_creds" {
  #checkov:skip=CKV_AWS_149:We no pay for xtra KMS
  #checkov:skip=CKV2_AWS_57:We don't need rotation
  name        = "gitlab-container-registry-creds"
  description = "Allows ECS to retrieve images from our private Gitlab Container registry."
  kms_key_id  = "alias/aws/secretsmanager"

  recovery_window_in_days = 0
}

resource "aws_secretsmanager_secret_version" "glcr_creds" {
  secret_id = aws_secretsmanager_secret.glcr_creds.id
  secret_string = jsonencode({
    username : gitlab_deploy_token.ecs_pull_images.username
    password : gitlab_deploy_token.ecs_pull_images.token
  })
}

resource "gitlab_group_variable" "glcr_aws_secret_arn" {
  group     = data.gitlab_group.el_capitano.full_path
  key       = "ECG_DEV_REVIEW_APPS_AWS_SECRET_MANAGER_GLCR_ARN"
  value     = aws_secretsmanager_secret_version.glcr_creds.arn
  protected = false
  masked    = true
}
