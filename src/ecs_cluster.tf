module "cluster_label" {
  source     = "git::https://github.com/cloudposse/terraform-null-label.git?ref=0.25.0"
  context    = module.default_label.context
  attributes = ["ecs", "cluster"]
}

resource "aws_ecs_cluster" "review_apps" {
  #checkov:skip=CKV_AWS_65:Container insights bring a lot of costs and dev isn't really the place for them.
  name = module.cluster_label.id

  setting {
    name  = "containerInsights"
    value = "disabled"
  }

  tags = module.cluster_label.tags
}

resource "aws_ecs_cluster_capacity_providers" "review_apps_cluster" {
  cluster_name = aws_ecs_cluster.review_apps.name

  capacity_providers = ["FARGATE_SPOT", "FARGATE"]

  default_capacity_provider_strategy {
    capacity_provider = "FARGATE_SPOT"
    weight            = 1
  }
}

resource "gitlab_group_variable" "aws_ecs_cluster_arn" {
  group     = data.gitlab_group.el_capitano.full_path
  key       = "ECG_DEV_REVIEW_APPS_AWS_CLUSTER_ARN"
  value     = aws_ecs_cluster.review_apps.arn
  protected = false
  masked    = true
}
