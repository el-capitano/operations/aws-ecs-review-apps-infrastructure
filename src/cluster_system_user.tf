module "review_system_user" {
  context    = module.default_label.context
  source     = "git::https://github.com/cloudposse/terraform-aws-iam-system-user?ref=1.2.1"
  attributes = ["system", "user"]

  inline_policies_map = {
    manage_review_apps = data.aws_iam_policy_document.manage_cluster.json
  }
  ssm_enabled = false
}

data "aws_iam_policy_document" "manage_cluster" {
  #checkov:skip=CKV_AWS_109:This system user needs to create roles. In real situation this would be put in separate AWS Account, preventing escalation
  #checkov:skip=CKV_AWS_110:This system user needs to create roles. In real situation this would be put in separate AWS Account, preventing escalation
  #checkov:skip=CKV_AWS_111:This system user needs to create roles. In real situation this would be put in separate AWS Account, preventing escalation
  #checkov:skip=CKV_AWS_356:This system user needs to create resources freely.
  statement {
    sid = "AllowUserToManageReviewApps"

    actions = [
      "application-autoscaling:*",
      "cloudformation:*",
      "ec2:AuthorizeSecurityGroupIngress",
      "ec2:CreateSecurityGroup",
      "ec2:CreateTags",
      "ec2:CreateNetworkInterface",
      "ec2:DeleteSecurityGroup",
      "ec2:DescribeNetworkInterfaces",
      "ec2:DescribeRouteTables",
      "ec2:DescribeSubnets",
      "ec2:DescribeSecurityGroups",
      "ec2:DescribeVpcs",
      "ec2:RevokeSecurityGroupIngress",
      "ecs:CreateCluster",
      "ecs:CreateService",
      "ecs:DeleteCluster",
      "ecs:DeleteService",
      "ecs:DeregisterTaskDefinition",
      "ecs:DescribeClusters",
      "ecs:DescribeServices",
      "ecs:DescribeTaskDefinition",
      "ecs:DescribeTasks",
      "ecs:ListAccountSettings",
      "ecs:ListTasks",
      "ecs:RegisterTaskDefinition",
      "ecs:TagResource",
      "ecs:UpdateService",
      "elasticfilesystem:CreateAccessPoint",
      "elasticfilesystem:CreateFileSystem",
      "elasticfilesystem:CreateMountTarget",
      "elasticfilesystem:CreateTags",
      "elasticfilesystem:DeleteAccessPoint",
      "elasticfilesystem:DeleteFileSystem",
      "elasticfilesystem:DeleteMountTarget",
      "elasticfilesystem:DescribeAccessPoints",
      "elasticfilesystem:DescribeBackupPolicy",
      "elasticfilesystem:DescribeFileSystemPolicy",
      "elasticfilesystem:DescribeFileSystems",
      "elasticfilesystem:DescribeLifecycleConfiguration",
      "elasticfilesystem:DescribeMountTargetSecurityGroups",
      "elasticfilesystem:DescribeMountTargets",
      "elasticloadbalancing:*",
      "iam:AttachRolePolicy",
      "iam:CreateRole",
      "iam:DeleteRole",
      "iam:DeleteRolePolicy",
      "iam:DetachRolePolicy",
      "iam:getRolePolicy",
      "iam:PutRolePolicy",
      "iam:PassRole",
      "iam:TagRole",
      "logs:CreateLogGroup",
      "logs:DeleteLogGroup",
      "logs:DescribeLogGroups",
      "logs:FilterLogEvents",
      "logs:PutRetentionPolicy",
      "route53:CreateHostedZone",
      "route53:DeleteHostedZone",
      "route53:GetHealthCheck",
      "route53:GetHostedZone",
      "route53:ListHostedZonesByName",
      "servicediscovery:*",
    ]

    resources = ["*"]
  }
}

# Add Gitlab CICD system user variables to the root group. These can then cascade to other projects under the root group and used with the Review Apps Template
resource "gitlab_group_variable" "cicd_access_key" {
  group     = data.gitlab_group.el_capitano.full_path
  key       = "ECG_DEV_REVIEW_APPS_USER_AWS_ACCESS_KEY_ID"
  value     = module.review_system_user.access_key_id
  protected = false
  masked    = true
}

resource "gitlab_group_variable" "cicd_secret_key" {
  group     = data.gitlab_group.el_capitano.full_path
  key       = "ECG_DEV_REVIEW_APPS_USER_AWS_SECRET_ACCESS_KEY"
  value     = module.review_system_user.secret_access_key
  protected = false
  masked    = true
}
