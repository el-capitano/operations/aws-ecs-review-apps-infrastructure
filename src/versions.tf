terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "5.90"
    }
    gitlab = {
      source  = "gitlabhq/gitlab"
      version = "17.9.0"
    }
  }
  required_version = "1.11.1"
}
