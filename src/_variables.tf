variable "account_id" {
  type        = string
  description = "The account ID of where the infrastructure is going to be provisioned."
}

variable "label" {
  type = object({
    namespace = string
    stage     = string
    name      = string
  })
  description = "Common labels to be used in this solution."
}

variable "role_name" {
  type        = string
  description = "Role for Terraform to assume"
  default     = null
}

variable "session_name" {
  type        = string
  description = "Session name when assuming AWS role. Set by the Terraform base jobs include by default."
  default     = "NOT_SET"
}

variable "gitlab_token" {
  type        = string
  description = "Gitlab token to use when using the Gitlab Provider."
}
