terraform {
  backend "http" {
    address        = "https://gitlab.com/api/v4/projects/26478995/terraform/state/review"
    lock_address   = "https://gitlab.com/api/v4/projects/26478995/terraform/state/review/lock"
    unlock_address = "https://gitlab.com/api/v4/projects/26478995/terraform/state/review/lock"
    username       = "gitlab-ci-token"
    lock_method    = "POST"
    unlock_method  = "DELETE"
    retry_wait_min = 5
  }
}

module "default_label" {
  source    = "git::https://github.com/cloudposse/terraform-null-label.git?ref=0.25.0"
  namespace = var.label.namespace
  stage     = var.label.stage
  name      = var.label.name
}

data "gitlab_group" "el_capitano" {
  full_path = "el-capitano"
}


